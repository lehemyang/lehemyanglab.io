---
layout: post
title: "使用git hook远程部署"
date: 2017-04-01 20:30:00
author: Lehem
category: Dev
tags: [PHP]
---

# 前言

在实际生产环境中，我们会将应用部署在服务器上。在本地完成一次git推送之后，就需要登录到生产环境的服务器，使用`git pull`命令从代码仓库拉取代码来实现功能更新。但是这样子有个麻烦的地方就是一旦有新的推送，每次都需要登录到服务器然后拉取代码，本文提供一个自动远程部署的方法，实现只要本地推送就可以远程自动部署。

# 环境

- linux系统，本文采用的是 CentOS 6.5 阿里云ECS
- 安装git
- 本地git环境，本文使用 OSX sierra

# 服务器创建git仓库
### 选择一：
{% highlight bash %}
$ cd ~                 // 进入到想要创建仓库的目录
$ mkdir yourRepoName   // 你的项目名称
$ cd yourRepoName
$ git --bare init      // --bare 选项是创建裸仓库，裸仓库没有工作区，作为部署的仓库不需要工作区
{% endhighlight %}
### 选择二:
如果部署仓库已经存在，不需要创建裸仓库的，使用以下命令将部署仓库变为可推送的
{% highlight bash %}
$ git config receive.denyCurrentBranch ignore
$ git config --bool receive.denyNonFastForwards false
{% endhighlight %}


# 在hook添加文件
{% highlight bash %}
$ cd .git/hooks
$ vim post-update   //post-update的作用就是，每次收到push的代码的时候，都会执行文件内的脚本命令
{% endhighlight %}

在post-update加入如下内容
{% highlight bash %}
#!/bin/sh
#
# This hook does two things:
#
#  1. update the "info" files that allow the list of references to be
#     queries over dumb transports such as http
#
#  2. if this repository looks like it is a non-bare repository, and
#     the checked-out branch is pushed to, then update the working copy.
#     This makes "push" function somewhat similarly to darcs and bzr.
#
# To enable this hook, make this file executable by "chmod +x post-update".

git-update-server-info

is_bare=$(git-config --get --bool core.bare)

if [ -z "$is_bare" ]
then
    # for compatibility's sake, guess
    git_dir_full=$(cd $GIT_DIR; pwd)
    case $git_dir_full in */.git) is_bare=false;; *) is_bare=true;; esac
fi

update_wc() {
    ref=$1
    echo "Push to checked out branch $ref" >&2
    if [ ! -f $GIT_DIR/logs/HEAD ]
    then
        echo "E:push to non-bare repository requires a HEAD reflog" >&2
        exit 1
    fi
    if (cd $GIT_WORK_TREE; git-diff-files -q --exit-code >/dev/null)
    then
        wc_dirty=0
    else
        echo "W:unstaged changes found in working copy" >&2
        wc_dirty=1
        desc="working copy"
    fi
    if git diff-index --cached HEAD@{1} >/dev/null
    then
        index_dirty=0
    else
        echo "W:uncommitted, staged changes found" >&2
        index_dirty=1
        if [ -n "$desc" ]
        then
            desc="$desc and index"
        else
            desc="index"
        fi
    fi
    if [ "$wc_dirty" -ne 0 -o "$index_dirty" -ne 0 ]
    then
        new=$(git rev-parse HEAD)
        echo "W:stashing dirty $desc - see git-stash(1)" >&2
        ( trap 'echo trapped $$; git symbolic-ref HEAD "'"$ref"'"' 2 3 13 15 ERR EXIT
        git-update-ref --no-deref HEAD HEAD@{1}
        cd $GIT_WORK_TREE
        git stash save "dirty $desc before update to $new";
        git-symbolic-ref HEAD "$ref"
        )
    fi

    # eye candy - show the WC updates :)
    echo "Updating working copy" >&2
    (cd $GIT_WORK_TREE
    git-diff-index -R --name-status HEAD >&2
    git-reset --hard HEAD)
}

if [ "$is_bare" = "false" ]
then
    active_branch=`git-symbolic-ref HEAD`
    export GIT_DIR=$(cd $GIT_DIR; pwd)
    GIT_WORK_TREE=${GIT_WORK_TREE-..}
    for ref
    do
        if [ "$ref" = "$active_branch" ]
        then
            update_wc $ref
        fi
    done
fi
{% endhighlight %}

不要忘了修改权限

{% highlight bash %}
$ chmod +x post-update
{% endhighlight %}

# 本地推送代码
在你的本地git仓库推送代码

{% highlight bash %}
$ echo "# test hook" >> README.md
$ git add README.md
$ git commit -m "test hook"
$ git remote add origin https://xxx/yourRepoName.git  // 对应你自己的git仓库
$ git push -u origin master
{% endhighlight %}

然后登录到远程服务器检查是不是代码已经自动在推送的分支上了，至此完成

# 注意事项
如果遇到不能推送的问题，注意检查git仓库权限是否正确。
使用`ll -a`命令确认当前目录下所有文件的权限
使用 `chown` 命令修改拥有者和组别， `-R` 表示递归修改`yourRepoName`目录已经所有子目录和文件

{% highlight bash %}
$ sudo chown -R myowner:mygroup yourRepoName
{% endhighlight %}


参考：

[Git Post-Receive Hook for Website Staging][Stack Overflow]

[搭建Git服务器][git tutorial]

[Stack Overflow]:     https://stackoverflow.com/questions/3838727/git-post-receive-hook-for-website-staging/8720786
[git tutorial]:    https://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000/00137583770360579bc4b458f044ce7afed3df579123eca000
