---
layout: post
title: "优雅实现无限级分类数据库"
date: 2017-05-17 20:30:00
author: Lehem
category: Dev
tags: [PHP]
---

# 前言

在实际开发当中肯定遇到过给对象分分类层级的问题，比如商品有一级二级三级甚至多级分类，或者公司组织架构也有一个部门分管多个部门，多个部门下面还有多个部门。分析下这个需求，其实就是把一个多叉树用数据库表示的问题。我搜索过很多方法，都有瑕疵，在这里给大家介绍一个比较优雅的方案。

# 常用解决方案

一般情况下，如果业务不是太复杂，只要用`category_id`、`parent_id`的方式解决，数据结构如下

|------------+----------+-------|
|category_id |parent_id |name   |
|------------|----------|-------|
| 1          | NULL     | one   |
| 2          | 1        | two   |
| 3          | 2        | three |
| 4          | 1        | four  |
| 5          | 4        | five  |
| 6          | 4        | six   |
| 7          | 6        | seven |
|------------|----------|-------|

但是这个方法有个致命的缺陷，如果一个末端节点想要跨越多层找到起始祖先节点，需要经过多个`where`语句，层级一旦多了只是想找祖先的`id`和`name`，结果跑了10句sql语句，这样子的体验非常糟糕。想要找出分类一共有几级也是同理。

# 优雅方法

我要介绍的方法叫闭包表（Closure Table）

# 服务器创建git仓库
<div class="hl">
$ cd ~                 // 进入到想要创建仓库的目录
$ mkdir yourRepoName   // 你的项目名称
$ cd yourRepoName
$ git --bare init      // --bare 选项是创建裸仓库，裸仓库没有工作区，作为部署的仓库不需要工作区
</div>
如果部署仓库已经存在，不需要创建裸仓库的，使用以下命令将部署仓库变为可推送的
<div class="hl">
$ git config receive.denyCurrentBranch ignore
$ git config --bool receive.denyNonFastForwards false
</div>

# 在hook添加文件


参考：

[Git Post-Receive Hook for Website Staging][Stack Overflow]

[搭建Git服务器][git tutorial]

[Stack Overflow]:     http://stackoverflow.com/questions/3838727/git-post-receive-hook-for-website-staging/8720786
[git tutorial]:    http://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000/00137583770360579bc4b458f044ce7afed3df579123eca000
