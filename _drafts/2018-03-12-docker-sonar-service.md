---
layout: post
title: "使用 Docker 快速部署 Sonarqube 服务"
date: 2018-03-12 11:38:00
author: Lehem
category: Dev
tags: [Docker]
---

# 说在前面
搜索到一些靠前的内容，提到如何部署 Sonarqube 的时候，都是分开安装 Sonarqube 的需要环境的，即使 Docker 也是如此，写这篇的目的是提供一个快速部署的方法。

# Docker 部署
找到 [Sonar官方镜像](https://hub.docker.com/_/sonarqube/)，根据描述我们选择使用 LTS 版本。

编写 Dockerfile
```
FROM sonarqube:lts
```
构建镜像
```
docker build -t lehem/sonarqube:lts ~/path_to_folder
```
运行镜像，注意替换数据库连接的信息。下面这个是官方的做法，但是存在一些错误。
```
docker run -d --name sonarqube \
    -p 9000:9000 -p 9092:9092 \
    -e SONARQUBE_JDBC_USERNAME=sonar \
    -e SONARQUBE_JDBC_PASSWORD=sonar \
    -e SONARQUBE_JDBC_URL=jdbc:mysql://localhost:3306/sonar \
    lehem/sonarqube
```
因为 Sonarqube 是用 Java 写的，JDBC 连接需要设置一些必要属性，我们把命令改一下
```
docker run -d --name sonarqube \
    -p 9000:9000 -p 9092:9092 \
    -e SONARQUBE_JDBC_USERNAME=sonar \
    -e SONARQUBE_JDBC_PASSWORD=sonar \
    -e SONARQUBE_JDBC_URL=jdbc:mysql://localhost:3306/sonar?useUnicode=true&characterEncoding=utf8&rewriteBatchedStatements=true&useConfigs=maxPerformance&useSSL=false \
    lehem/sonarqube
```
在某些 shell 下面，带参数的路由会解析错误，可能需要给 SONARQUBE_JDBC_URL 这个字段的值前后加上`"`，让其整体是个字符串。
另外说明一下，这里的数据库连接参数不是必要填写的，虽然 Sonarqube 内置了数据库，但是建议使用外部专用的连接。

# Nginx 配置
编写配置文件
/etc/nginx/conf.d/sonarqube.conf
{% highlight nginx %}
server {
    listen        80;
    server_name   your.domain.com;

    location / {
        proxy_pass http://0.0.0.0:9000;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}
{% endhighlight %}
因为 Sonarqube 服务在本地 9000 端口，Nginx 的作用只是负责转发，重启服务器之后就可以访问服务了。
