---
layout: post
title: "decimal使用建议"
date: 2017-03-16 12:30:00
category: Dev
tags: [PHP, Ruby]
---

## decimal 应用场景
在编写财务，电子商务之类应用的时候，经常会碰到小数，小数乘、除、加、减的场景。
在阿里巴巴最新发布的[阿里巴巴Java开发手册（正式版）][manual]里面已经明确提到：

> 小数类型为 decimal，禁止使用 float 和 double。

> 说明:float 和 double 在存储的时候，存在精度损失的问题，很可能在值的比较时，得到不 正确的结果。如果存储的数据范围超过 decimal 的范围，建议将数据拆成整数和小数分开存储。

[manual]:     https://yq.aliyun.com/articles/69327
