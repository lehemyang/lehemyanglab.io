---
layout: post
title: "Laravel/Lumen PHPUnit  使用心得"
date: 2017-08-21 21:38:00
author: Lehem
category: Dev
tags: [PHP]
---

# 使用文档  
[PHPUnit 官方文档](https://phpunit.de/manual/6.3/zh_cn/installation.html#installation.composer)已经写得很详细了,就不写重复内容了.我这里要写一些生产当中重要的内容  
[laravel 测试文档](http://d.laravel-china.org/docs/5.4/http-tests)主要增加一些 Laravel 下可以使用的一些断言(assert)
  


# 单元测试
我们就是希望项目变得更好维护,代码不出错误,单元测试就是这样的工具.  
通常,我们单元测试帮我们检测某些功能模块/函数方法的正确性.但是在机器的机制里,它只是按照指令运行的,你觉得有 bug, 注意,那是你定义的"bug",机器从来不会觉得出了 bug.  
所以,我们需要用单元测试断言方法来告诉机器,哪些测试结果是对的,而哪些是错误的.  
  
举个例子,我们使用一个`assertEquals`断言方法,当传入的两个参数相等的时候,测试通过,否则不通过.

{% highlight php %}

   /** 生成数据*/
   $target = [1,2,3];
   $expected = 3;

   /** 运行 */
   $actual = count($target);

   /** 断言 */
   $this->assertEquals($expected, $actual);

{% endhighlight %}
看上面这个例子,`$expected`是我们告诉断言方法我们期望结果是怎么样的,然后我们对一个`$target`进行`count`计算得到一个结果,如果这个结果和我们预期相等,那么测试就算通过.  
充分利用 PHPUnit 和 Laravel 提供的断言方法可以让测试变得轻松.


# 运行测试

根据文档自己写测试类,注意 Lumen 下不支持使用`make:test`生成测试类,可以复制`ExampleTest.php`生成自己的测试类.

{% highlight bash %}
$ cd yourProject       // 进入到项目的目录
$ phpunit              // 运行所有 Test 结尾的 php 文件的所有 test 开头的方法
$ vendor/bin/phpunit   // 在 Lumen 下可能需要使用这个命令测试
{% endhighlight %}

# 指定文件/目录运行
有些场景就是我们并不需要对所有文件/方法进行测试,官网这点写的不详细,补充一些内容.
**测试指定目录下文件**
{% highlight bash %}
$ phpunit tests/myfolder        // 运行 tests/myfolder 目录下所有 Test 结尾的 php 文件
{% endhighlight %}

**测试指定文件**
{% highlight bash %}
$ phpunit --filter myFileTest   // 测试名为 myFileTest.php 的文件
{% endhighlight %}

**测试指定方法**
运行名为 myMethodTest 的方法,方法不能重名,否则后面需要加上文件名或者目录名保证方法名唯一性
{% highlight bash %}
$ phpunit --filter myMethodTest
$ phpunit --filter myMethodTest tests/myFileTest
{% endhighlight %}

# 与 dingo/api 搭配使用可能出现的问题
如果在测试中使用了 dingo/api 以及 json/get/等 模拟 http 请求,如下

{% highlight php %}
   $this->get('/');
   $response = $this->json('POST', '/user', ['name' => 'Sally']);
{% endhighlight %}

可能会收到如下的报错提示
{% highlight bash %}
// 提示未注册路由或者不存在
{"message":"The version given was unknown or has no registered routes.","status_code":400,"debug ...
// 或者类似下面这样子,在 CLI 里面就是类似纯 HTML 输出,大概意思是 NotFoundHttpException 
'<!DOCTYPE html>\n
<html>\n
    <head>\n
        <meta name="robots" content="noindex,nofollow" />\n
        <style>\n
{% endhighlight %}


一个可能的解决办法(Lumen亲测可用)就是修改配置文件`phpunit.xml`,
删除`bootstrap="bootstrap/app.php`这行,将 `processIsolation="false"` 改为 `processIsolation="true"`.
