# 开发事项

## 文件目录
> _config.yml

这个是所有配置信息的存放处

## 命令
`rbenv install -l `
`rbenv install 3.0.0`
`rbenv global 3.0.0`
`gem env home`
`gem install jekyll`

gem 依赖版本太低的话, 类似 pod update
`bundle update`

Jekyll serve fails on Ruby 3.0 · Issue #8523 · jekyll/jekyll
https://github.com/jekyll/jekyll/issues/8523

bundle add webrick


运行预览
`bundle exec jekyll serve`

运行带有草稿的预览
`bundle exec jekyll serve --drafts`



Gemfile 的源在开发期可以改成  `source "https://gems.ruby-china.com/"`
这样子会快点



## 待办 ❗️✅
✅️定制首页, 去掉图片, 改为文章列表
❗️修改评论系统, 使用 github 的评论
✅找到趁手的 markdown 编写工具 in Jetbrains
✅❗在 Page 侧边页生成 TOC
✅生成 typography
✅更新 ruby 版本 jekyll 环境
❗页面访问太慢, chrome 检查下 network 
❗页面支持 doge 表情
❗oss 上传阿里云或者腾讯云，发布的时候修改指向
❗找到调试 HTML 页面参数的办法，最好有断点调试
❗更新友链

blog/index.html 的文件复制到 index.html 下无效

```
---
layout: default
title: Home
---
```
这里面的 layout 什么用
这里面的 title 什么用


## 其它
1. 在 html 文件里面写备注, 会在 Chrome "显示网页源代码"里面被看到
2. 写在 html 里面的变量都会被执行, 就算注释了也会被执行,比如加入了`<p>paginator.page {{paginator.page}}</p>`
3. 域名刚进入, 会默认访问 index.html 文件
4. 摘录分隔符, _config.yaml, excerpt_separator 变量
5. 

## 善于使用变量
`http://jekyllcn.com/docs/variables/`
减少代码实现, 比如我可以直接在 index.html 里面写如下代码, 直接生成页面
```html
---
<!--layout: default-->
title: Home
---
{{site.posts}}
```

## 待写文章
### 必写
- iOS 启动优化 ✅
- 后端一篇, 大型网站架构抽离
- Flutter 挑出核心技术实战的内容
- Flutter 引擎编译改动相关的内容
- html 渲染的路径到
- 用内容渲染到 iOS skia
- 技术文章翻译
- 管理相关, 软件作坊里面的内容
- 上东家的思考和总结
- 知行的一些内容
- 重来里面的内容整合, 软件作坊里面的内容
- 人月神话读书的笔记
- 美国游记 ✅
- 以色列游记
- ts 学习
- v8 学习
- koa 学习 以及一系列后端框架
- autorelease 的真实情况

### 选写
- iOS 链接器
- fair 类似实现的文章
- 京东阅读里面的内容, 对内容进行加密
- 翻新以前的文章
- Google 工作法
- 写出一篇解释 dart 继承 mix abstract 等特性的测试内容, 双分派那样子测试
- WWDC 之类的大会视频的笔记
